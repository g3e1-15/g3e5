//GUIA 3 EJERCICIO 5
//El usuario ingresa 5 edades (enteras) y 5 sueldos (float), correspondientes a 5 personas.
//Muestre en pantalla los vectores con los valores ingresados.
//Muestre en qu� posici�n se encuentra la persona de m�s edad y en qu� posici�n se encuentra el mayor sueldo.

#include <stdio.h>

int main()
{
int Sueldomax = 0;
int Edadmax = 0;
float Sueldo[5];
int Edad[5];
int i, Posicionsueldo, Posicionedad;

for (i = 0; i<=4 ; i++)
   {
       printf("Ingresar la edad de la persona N%d:",i);
       scanf("%d",&Edad[i]);
       printf("Ingresar el sueldo de la persona N%d:",i);
       scanf("%f",&Sueldo[i]);
   }
printf("\n");
for (i = 0; i<=4 ; i++)
   {
       printf("La edad ingresada en el vector %d es %d\n",i , Edad[i]);
       printf("El sueldo ingresado en el vector %d es %.2lf\n",i , Sueldo[i]);
   }
for (i = 0; i<=4 ; i++)
   {
       if(Edad[i] > Edadmax)
       {
           Edadmax = Edad[i];
           Posicionedad = i;
       }
   }
printf("\n");
for (i = 0 ; i<=4 ; i++)
   {
      if(Sueldo[i] > Sueldomax)
       {
           Sueldomax = Sueldo[i];
           Posicionsueldo = i;
       }
   }
       printf("La mayor edad ingresada se encuentra en el vector %d\n",Posicionedad);
       printf("El mayor sueldo ingresado se encuentra en el vector %d\n",Posicionsueldo);
}
